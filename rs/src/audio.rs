use bevy::audio::{PlaybackMode, Volume, VolumeLevel};
use bevy::prelude::*;
use rand::prelude::SliceRandom;
use rand::{thread_rng, Rng};

pub fn setup(mut cmds: Commands, assets: Res<AssetServer>, mut vol: ResMut<GlobalVolume>) {
	vol.volume = VolumeLevel::new(0.5);

	let bgm = assets.load("bgm_Loop.ogg");
	cmds.spawn(AudioBundle {
		source: bgm,
		settings: PlaybackSettings {
			mode: PlaybackMode::Loop,
			volume: Volume::Relative(VolumeLevel::new(0.5)),
			..default()
		},
	});

	let crunches = Crunches(vec![
		assets.load("crunch_1_Loop.wav"),
		assets.load("crunch_2_Loop.wav"),
		assets.load("crunch_3_Loop.wav"),
	]);
	cmds.insert_resource(crunches);
}

#[derive(Resource, Debug, Deref, DerefMut)]
pub struct Crunches(pub Vec<Handle<AudioSource>>);

impl Crunches {
	pub fn random(&self) -> Handle<AudioSource> {
		self.random_using(&mut thread_rng())
	}

	pub fn random_using(&self, rng: &mut impl Rng) -> Handle<AudioSource> {
		self.0
			.choose(rng)
			.expect("choose a random crunch sound") // I should never get rid of the handles
			.clone()
	}
}
