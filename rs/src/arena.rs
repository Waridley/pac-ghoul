use bevy::prelude::*;

use std::ops::{Index, IndexMut};
use std::time::Duration;

use petgraph::prelude::UnGraphMap;

use crate::collision::{Collider, Sensor};
use crate::player::IsPlayer;
use crate::sim::SimPos;
use rand::random;

pub const ROWS: usize = 12;
pub const COLUMNS: usize = 16;
pub const TILE_SIZE: Vec2 = Vec2 { x: 24.0, y: 24.0 };

pub const MIN_SPROUT_SECS: f32 = 5.0;
pub const MAX_SPROUT_SECS: f32 = 180.0;

pub fn setup(
	mut cmds: Commands,
	assets: Res<AssetServer>,
	mut texture_atlases: ResMut<Assets<TextureAtlas>>,
) {
	let plant_sprite = assets.load("plant.png");
	let plant_sprite =
		TextureAtlas::from_grid(plant_sprite, Vec2::new(24.0, 24.0), 7, 1, None, None);
	let plant_sprite = texture_atlases.add(plant_sprite);

	let small_bean = assets.load("bean_8.png");
	let large_bean = assets.load("bean_16.png");
	let mut arena = Arena {
		plant_sprite,
		small_bean,
		large_bean,
		..default()
	};
	let mut path_data = PathData::default();

	for (x, row) in arena.grid.iter_mut().enumerate() {
		for (y, tile) in row.iter_mut().enumerate() {
			let id = cmds
				.spawn((
					SpriteSheetBundle {
						sprite: TextureAtlasSprite::new(0),
						texture_atlas: arena.plant_sprite.clone(),
						transform: Transform::from_xyz(0.0, 0.0, -1.0),
						..default()
					},
					SimPos::from_tile_index(x, y),
					TileIndex(x, y),
				))
				.id();
			tile.entity = id;

			let _i = path_data.add_node((x, y));
			if y < ROWS - 1 {
				path_data.add_edge((x, y), (x, y + 1), ());
			}
			if y > 0 {
				path_data.add_edge((x, y), (x, y - 1), ());
			}
			if x > 0 {
				path_data.add_edge((x, y), (x - 1, y), ());
			}
			if x < COLUMNS - 1 {
				path_data.add_edge((x, y), (x + 1, y), ());
			}
		}
	}

	// Walls
	cmds.spawn((
		SimPos::new(0.0, (ROWS as f32 + 1.0) / 2.0),
		Collider::Rect {
			x: COLUMNS as f32,
			y: 1.0,
		},
	));

	cmds.spawn((
		SimPos::new(0.0, -(ROWS as f32 + 1.0) / 2.0),
		Collider::Rect {
			x: COLUMNS as f32,
			y: 1.0,
		},
	));

	cmds.spawn((
		SimPos::new(-(COLUMNS as f32 + 1.0) / 2.0, 0.0),
		Collider::Rect {
			x: 1.0,
			y: ROWS as f32,
		},
	));

	cmds.spawn((
		SimPos::new((COLUMNS as f32 + 1.0) / 2.0, 0.0),
		Collider::Rect {
			x: 1.0,
			y: ROWS as f32,
		},
	));

	cmds.insert_resource(arena);
	cmds.insert_resource(path_data);

	let mut spawn_timer = PlantSpawnTimer(Timer::from_seconds(10.0, TimerMode::Repeating));
	spawn_timer.tick(Duration::from_secs_f32(9.99)); // First spawn should happen immediately
	cmds.insert_resource(spawn_timer);
}

#[derive(Resource, Debug, Default)]
pub struct Arena {
	pub grid: [[Tile; ROWS]; COLUMNS],
	pub plant_sprite: Handle<TextureAtlas>,
	pub small_bean: Handle<Image>,
	pub large_bean: Handle<Image>,
}

impl Arena {
	fn spawn_plant(&mut self, path_data: &mut PathData) -> Option<(usize, usize)> {
		let cell = random::<usize>() % ((ROWS - 2) * (COLUMNS - 2));
		let x = (cell % (COLUMNS - 2)) + 1;
		let y = (cell / (COLUMNS - 2)) + 1;
		if self.grid[x][y].resident == TileResident::None {
			let mut tmp = path_data.clone();

			// remove the tile where we want to spawn a plant
			tmp.remove_node((x, y));

			// make sure the field is connected by at least one inner path, not just accessible from the outside
			for x in 0..COLUMNS {
				tmp.remove_node((x, 0));
				tmp.remove_node((x, ROWS - 1));
			}
			for y in 0..ROWS {
				tmp.remove_node((0, y));
				tmp.remove_node((COLUMNS - 1, y));
			}

			if petgraph::algo::connected_components(&*tmp) == 1 {
				println!("adding plant at {{ {x}, {y} }}");
				self.grid[x][y].resident = TileResident::Plant;
				path_data.remove_node((x, y));
				return Some((x, y));
			} else {
				eprintln!("{{ {x}, {y} }} would disconnect graph");
			}
		}
		None
	}

	pub fn closest_tile_to(&self, pos: SimPos) -> TileIndex {
		let x = (pos.x + ((COLUMNS - 1) as f32 / 2.0)).round() as usize;
		let y = (pos.y + ((ROWS - 1) as f32 / 2.0)).round() as usize;
		TileIndex(x, y)
	}
}

impl Index<TileIndex> for Arena {
	type Output = Tile;

	fn index(&self, index: TileIndex) -> &Self::Output {
		&self.grid[index.0][index.1]
	}
}

impl IndexMut<TileIndex> for Arena {
	fn index_mut(&mut self, index: TileIndex) -> &mut Self::Output {
		&mut self.grid[index.0][index.1]
	}
}

#[derive(Resource, Deref, DerefMut, Default, Clone)]
pub struct PathData(UnGraphMap<(usize, usize), ()>);

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Tile {
	pub entity: Entity,
	pub resident: TileResident,
}

impl Default for Tile {
	fn default() -> Self {
		Self {
			entity: Entity::PLACEHOLDER,
			resident: TileResident::default(),
		}
	}
}

#[derive(Default, Debug, PartialEq, Eq, Clone)]
pub enum TileResident {
	#[default]
	None,
	Plant,
	SmallBean(Entity),
	LargeBean(Entity),
	Player,
	Other(Entity),
}

impl Tile {
	pub fn is_empty(&self) -> bool {
		self.resident == TileResident::None
	}
}

#[derive(Component, Default, Debug, PartialEq, Eq, Clone, Copy)]
pub struct TileIndex(pub usize, pub usize);

pub fn update_residents(
	positions: Query<&SimPos>,
	q: Query<(Entity, &SimPos, &Collider, Option<&IsPlayer>)>,
	all_entities: Query<()>,
	mut arena: ResMut<Arena>,
) {
	for x in 0..COLUMNS {
		for y in 0..ROWS {
			if matches!(
				arena.grid[x][y].resident,
				TileResident::None | TileResident::Player | TileResident::Other(..)
			) {
				let tile_pos = positions
					.get(arena.grid[x][y].entity)
					.expect("get tile entity"); // They should never be despawned until the game is exited
				let mut occupied = false;
				for (entity, pos, col, is_player) in &q {
					if col.distance_to_collide_with(Collider::default(), *pos, *tile_pos) < 0.0 {
						if is_player.is_some() {
							arena.grid[x][y].resident = TileResident::Player;
						} else {
							arena.grid[x][y].resident = TileResident::Other(entity);
						}
						occupied = true;
						break;
					}
				}
				if !occupied {
					arena.grid[x][y].resident = TileResident::None;
				}
			}
			match arena.grid[x][y].resident {
				TileResident::SmallBean(entity)
				| TileResident::LargeBean(entity)
				| TileResident::Other(entity) => {
					if !all_entities.contains(entity) {
						eprintln!("Previous resident {entity:?} is invalid");
						arena.grid[x][y].resident = TileResident::None;
					}
				}
				_ => {}
			}
		}
	}
}

pub fn spawn_plants(
	mut cmds: Commands,
	mut q: Query<&mut TextureAtlasSprite>,
	mut arena: ResMut<Arena>,
	mut path_data: ResMut<PathData>,
	mut timer: ResMut<PlantSpawnTimer>,
	t: Res<Time>,
) {
	timer.tick(t.delta());
	if timer.just_finished() {
		for _ in 0..4 {
			if let Some((x, y)) = arena.spawn_plant(&mut *path_data) {
				let id = arena.grid[x][y].entity.clone();
				let mut entity = cmds.entity(id);
				let mut atlas = q.get_mut(id).expect("get tile TextureAtlasSprite"); // Tile entities should never despawn until the game ends
				atlas.index = 1;
				entity.insert((
					Collider::default(),
					PlantGrowTimer(Timer::from_seconds(2.0, TimerMode::Once)),
					BeanSproutTimer(Timer::from_seconds(
						random::<f32>() * 5.0 + 2.0,
						TimerMode::Once,
					)),
					BeanDropTimer(Timer::from_seconds(2.0, TimerMode::Once)),
				));
			}
		}
	}
}

pub fn grow_plants(
	mut cmds: Commands,
	mut q: Query<(
		&mut PlantGrowTimer,
		&mut TextureAtlasSprite,
		&mut BeanSproutTimer,
		&mut BeanDropTimer,
		&TileIndex,
	)>,
	mut arena: ResMut<Arena>,
	t: Res<Time>,
) {
	fn find_neighbors(arena: &Arena, x: usize, y: usize) -> Vec<(usize, usize)> {
		let mut neighbors = Vec::new();
		if y < ROWS - 1 && arena.grid[x][y + 1].is_empty() {
			neighbors.push((x, y + 1))
		};
		if y > 0 && arena.grid[x][y - 1].is_empty() {
			neighbors.push((x, y - 1))
		};
		if x > 0 && arena.grid[x - 1][y].is_empty() {
			neighbors.push((x - 1, y))
		};
		if x < COLUMNS - 1 && arena.grid[x + 1][y].is_empty() {
			neighbors.push((x + 1, y))
		};
		neighbors
	}

	for (mut grow_timer, mut sprite, mut sprout_timer, mut drop_timer, TileIndex(x, y)) in &mut q {
		grow_timer.tick(t.delta());
		if grow_timer.finished() {
			if sprite.index < 4 {
				sprite.index += 1;
				grow_timer.reset();
			} else if sprite.index == 4 {
				sprout_timer.tick(t.delta());
				if sprout_timer.just_finished() {
					sprout_timer.set_duration(Duration::from_secs_f32(
						random::<f32>() * (MAX_SPROUT_SECS - MIN_SPROUT_SECS) + MIN_SPROUT_SECS,
					));
					sprout_timer.reset();
					if find_neighbors(&arena, *x, *y).len() == 0 {
						continue;
					}
					sprite.index = if random::<f32>() < 0.05 { 6 } else { 5 };
					drop_timer.reset();
				}
			} else {
				drop_timer.tick(t.delta());
				if drop_timer.just_finished() {
					drop_timer.reset();
					println!("Plant dropped bean at {{ {x}, {y} }}");
					let neighbors = find_neighbors(&arena, *x, *y);
					if neighbors.len() > 0 {
						let neighbor = random::<usize>() % neighbors.len();
						let (x, y) = neighbors[neighbor];
						let (texture, r, marker) = if sprite.index == 6 {
							(arena.large_bean.clone(), 0.5, Bean::Large)
						} else {
							(arena.small_bean.clone(), 0.2, Bean::Small)
						};
						sprite.index = 4;
						let id = cmds
							.spawn((
								SpriteBundle {
									texture,
									..default()
								},
								SimPos(Vec2::new(
									x as f32 - ((COLUMNS as f32) / 2.0) + 0.5,
									y as f32 - ((ROWS as f32) / 2.0) + 0.5,
								)),
								Sensor(Collider::Circle { r }),
								TileIndex(x, y),
								marker,
							))
							.id();
						arena.grid[x][y].resident = TileResident::SmallBean(id);
					} else {
						eprintln!("Something popped up where I was gonna put a bean!");
					}
				}
			}
		}
	}
}

#[derive(Resource, Deref, DerefMut)]
pub struct PlantSpawnTimer(pub Timer);

#[derive(Component, Deref, DerefMut)]
pub struct PlantGrowTimer(pub Timer);

#[derive(Component, Deref, DerefMut)]
pub struct BeanSproutTimer(pub Timer);

#[derive(Component, Deref, DerefMut)]
pub struct BeanDropTimer(pub Timer);

#[derive(Component, Debug, Copy, Clone)]
pub enum Bean {
	Small,
	Large,
}

pub const NUM_PERIMETER_TILES: usize = ((COLUMNS - 1) * 2) + ((ROWS - 1) * 2);
#[rustfmt::skip]
pub const PERIMETER_INDICES: [(usize, usize); NUM_PERIMETER_TILES] = [
	(0, 0), (0, 1), (0, 2), (0, 3), (0, 4), (0, 5), (0, 6), (0, 7), (0, 8), (0, 9), (0, 10), (0, 11),
	(1, 11), (2, 11), (3, 11), (4, 11), (5, 11), (6, 11), (7, 11), (8, 11), (9, 11), (10, 11), (11, 11), (12, 11), (13, 11), (14, 11), (15, 11),
	(15, 10), (15, 9), (15, 8), (15, 7), (15, 6), (15, 5), (15, 4), (15, 3), (15, 2), (15, 1), (15, 0),
	(14, 0), (13, 0), (12, 0), (11, 0), (10, 0), (9, 0), (8, 0), (7, 0), (6, 0), (5, 0), (4, 0), (3, 0), (2, 0), (1, 0),
];
