use crate::AppState;
use bevy::app::AppExit;
use bevy::audio::VolumeLevel;
use bevy::ecs::system::EntityCommands;
use bevy::prelude::*;
use bevy::window::PrimaryWindow;
use std::marker::PhantomData;

pub fn handle_pause_buttons(
	kb: Res<Input<KeyCode>>,
	state: Res<State<AppState>>,
	mut next_state: ResMut<NextState<AppState>>,
) {
	if kb.any_just_pressed([KeyCode::Escape, KeyCode::Return, KeyCode::NumpadEnter]) {
		next_state.set(match **state {
			AppState::Paused => AppState::UnPaused,
			AppState::UnPaused => AppState::Paused,
		});
	}
}

struct MenuBuilder<'w, 's, M: Menu> {
	cmds: Commands<'w, 's>,
	entities: Vec<Entity>,
	kind: PhantomData<M>,
}

impl<'w, 's, M: Menu> MenuBuilder<'w, 's, M> {
	fn new(cmds: Commands<'w, 's>) -> Self {
		Self {
			cmds,
			entities: vec![],
			kind: PhantomData::default(),
		}
	}

	fn spawn<'a, T: Bundle>(&'a mut self, bundle: T) -> Spawner<'w, 's, 'a> {
		let cmds = self.cmds.spawn(bundle);
		Spawner {
			entities: &mut self.entities,
			cmds,
		}
	}
}

impl<'w, 's, M: Menu> Drop for MenuBuilder<'w, 's, M> {
	fn drop(&mut self) {
		self.cmds.insert_resource(M::from_entities(
			std::mem::take(&mut self.entities).into_boxed_slice(),
		));
	}
}

struct Spawner<'w, 's, 'b> {
	entities: &'b mut Vec<Entity>,
	cmds: EntityCommands<'w, 's, 'b>,
}

impl<'w, 's, 'b> Drop for Spawner<'w, 's, 'b> {
	fn drop(&mut self) {
		self.entities.push(self.cmds.id());
	}
}

#[derive(Resource, Debug)]
pub struct PauseMenu {
	pub entities: Box<[Entity]>,
}

pub fn show_pause_menu(
	cmds: Commands,
	assets: Res<AssetServer>,
	texture_atlases: ResMut<Assets<TextureAtlas>>,
	volume: Res<GlobalVolume>,
) {
	spawn_pause_menu(cmds, assets, texture_atlases, volume);
}

fn spawn_pause_menu(
	cmds: Commands,
	assets: Res<AssetServer>,
	mut texture_atlases: ResMut<Assets<TextureAtlas>>,
	volume: Res<GlobalVolume>,
) {
	let font = assets.load("VT323-Regular.ttf");

	let background = assets.load("menu_bg.png");

	let mut cmds = MenuBuilder::<PauseMenu>::new(cmds);
	cmds.spawn((SpriteBundle {
		transform: Transform::from_xyz(0.0, 0.0, 900.0),
		texture: background,
		..default()
	},));

	cmds.spawn(Text2dBundle {
		text: Text::from_section(
			"Paused",
			TextStyle {
				font: font.clone(),
				font_size: 24.0,
				..default()
			},
		),
		transform: Transform::from_xyz(0.0, 84.0, 909.0),
		..default()
	});

	cmds.spawn(Text2dBundle {
		text: Text::from_section(
			"Volume:",
			TextStyle {
				font: font.clone(),
				font_size: 24.0,
				..default()
			},
		),
		transform: Transform::from_xyz(-60.0, 48.0, 909.0),
		..default()
	});

	let slider = assets.load("slider.png");

	cmds.spawn((SpriteBundle {
		texture: slider,
		transform: Transform::from_xyz(0.0, 24.0, 901.0),
		..default()
	},));

	let slider_handle = assets.load("slider_handle.png");
	let slider_handle =
		TextureAtlas::from_grid(slider_handle, Vec2::new(16.0, 24.0), 3, 1, None, None);
	let mut size = BtnSize(slider_handle.size);
	size.x /= 3.0; // atlas size is all frames
	let slider_handle = texture_atlases.add(slider_handle);
	cmds.spawn((
		BtnState::default(),
		size,
		SpriteSheetBundle {
			texture_atlas: slider_handle,
			transform: Transform::from_xyz(volume.volume.get() * 216.0 - 108.0, 24.0, 902.0),
			..default()
		},
		IsSliderHandle,
	));

	let btn = assets.load("btn.png");
	let btn = TextureAtlas::from_grid(btn, Vec2::new(60.0, 36.0), 3, 1, None, None);
	let mut size = BtnSize(btn.size);
	size.x /= 3.0; // atlas size is all frames
	let btn = texture_atlases.add(btn);
	#[cfg(not(target_arch = "wasm32"))]
	{
		cmds.spawn((
			BtnState::default(),
			size,
			SpriteSheetBundle {
				transform: Transform::from_xyz(-60.0, -72.0, 901.0),
				texture_atlas: btn.clone(),
				..default()
			},
			IsOkBtn,
		));
		cmds.spawn((
			BtnState::default(),
			size,
			SpriteSheetBundle {
				transform: Transform::from_xyz(60.0, -72.0, 901.0),
				texture_atlas: btn.clone(),
				..default()
			},
			IsQuitBtn,
		));

		cmds.spawn(Text2dBundle {
			text: Text::from_section(
				"Play",
				TextStyle {
					font: font.clone(),
					font_size: 24.0,
					..default()
				},
			),
			transform: Transform::from_xyz(-60.0, -70.0, 902.0),
			..default()
		});

		cmds.spawn(Text2dBundle {
			text: Text::from_section(
				"Quit",
				TextStyle {
					font,
					font_size: 24.0,
					..default()
				},
			),
			transform: Transform::from_xyz(60.0, -70.0, 902.0),
			..default()
		});
	}

	#[cfg(target_arch = "wasm32")]
	{
		cmds.spawn((
			BtnState::default(),
			size,
			SpriteSheetBundle {
				transform: Transform::from_xyz(0.0, -72.0, 901.0),
				texture_atlas: btn.clone(),
				..default()
			},
			IsOkBtn,
		));
		cmds.spawn(Text2dBundle {
			text: Text::from_section(
				"Play",
				TextStyle {
					font: font.clone(),
					font_size: 24.0,
					..default()
				},
			),
			transform: Transform::from_xyz(0.0, -70.0, 902.0),
			..default()
		});
	}
}

#[derive(Component, Debug)]
pub struct IsOkBtn;

#[derive(Component, Debug)]
pub struct IsQuitBtn;

#[derive(Component, Debug)]
pub struct IsYesBtn;

#[derive(Component, Debug)]
pub struct IsNoBtn;

#[derive(Component, Debug)]
pub struct IsSliderHandle;

#[derive(Component, Default, Debug, Copy, Clone, PartialEq, Eq)]
pub enum BtnState {
	#[default]
	Unpressed = 0,
	Focused = 1,
	Pressed = 2,
}

pub fn despawn_menus(
	mut cmds: Commands,
	pause_menu: Res<PauseMenu>,
	really_quit_menu: Option<Res<ReallyQuitMenu>>,
) {
	pause_menu.despawn(&mut cmds);
	if let Some(really_quit_menu) = really_quit_menu {
		really_quit_menu.despawn(&mut cmds);
	}
}

trait Menu: Resource {
	fn from_entities(entities: Box<[Entity]>) -> Self;
	fn entities(&self) -> &[Entity];

	fn despawn(&self, cmds: &mut Commands)
	where
		Self: Sized,
	{
		for id in self.entities() {
			cmds.entity(*id).despawn();
		}
		cmds.remove_resource::<Self>();
	}
}

impl Menu for PauseMenu {
	fn from_entities(entities: Box<[Entity]>) -> Self {
		Self { entities }
	}

	fn entities(&self) -> &[Entity] {
		&*self.entities
	}
}

#[derive(Resource)]
pub struct ReallyQuitMenu {
	pub entities: Box<[Entity]>,
}

impl Menu for ReallyQuitMenu {
	fn from_entities(entities: Box<[Entity]>) -> Self {
		Self { entities }
	}

	fn entities(&self) -> &[Entity] {
		&*self.entities
	}
}

pub fn update_btn_sprite(mut q: Query<(&mut TextureAtlasSprite, &BtnState)>) {
	for (mut sprite, state) in &mut q {
		sprite.index = *state as _;
	}
}

pub fn update_btn_state(
	mut q: Query<(&Transform, &mut BtnState, &BtnSize)>,
	window: Query<&Window, With<PrimaryWindow>>,
	camera: Query<(&Camera, &GlobalTransform)>,
	mouse_btns: Res<Input<MouseButton>>,
) {
	if let Some(pos) = window.single().cursor_position() {
		let (cam, cam_xform) = camera.single();
		let pos = cam
			.viewport_to_world(cam_xform, pos)
			.expect("mouse pos to world coords")
			.origin
			.truncate();
		for (xform, mut state, size) in &mut q {
			let (x, y) = (xform.translation.x, xform.translation.y);
			let new_state = if pos.x > x - (size.x / 2.0)
				&& pos.x < x + (size.x / 2.0)
				&& pos.y > y - (size.y / 2.0)
				&& pos.y < y + (size.y / 2.0)
			{
				if mouse_btns.pressed(MouseButton::Left) {
					BtnState::Pressed
				} else {
					BtnState::Focused
				}
			} else {
				BtnState::Unpressed
			};
			if *state != new_state {
				*state = new_state;
			}
		}
	}
}

pub fn handle_quit_btn(
	cmds: Commands,
	assets: Res<AssetServer>,
	mut texture_atlases: ResMut<Assets<TextureAtlas>>,
	q: Query<Ref<BtnState>, With<IsQuitBtn>>,
) {
	if let Ok(state) = q.get_single() {
		if state.is_added() || !state.is_changed() {
			return;
		}
		if *state == BtnState::Pressed {
			let mut cmds = MenuBuilder::<ReallyQuitMenu>::new(cmds);
			let background = assets.load("alert_bg.png");
			let font = assets.load("VT323-Regular.ttf");
			cmds.spawn((SpriteBundle {
				transform: Transform::from_xyz(0.0, 0.0, 950.0),
				texture: background,
				..default()
			},));

			let btn = assets.load("btn.png");
			let btn = TextureAtlas::from_grid(btn, Vec2::new(60.0, 36.0), 3, 1, None, None);
			let mut size = BtnSize(btn.size);
			size.x /= 3.0; // atlas size is all frames
			let btn = texture_atlases.add(btn);
			cmds.spawn((
				BtnState::default(),
				size,
				SpriteSheetBundle {
					transform: Transform::from_xyz(-60.0, -72.0, 951.0),
					texture_atlas: btn.clone(),
					..default()
				},
				IsYesBtn,
			));
			cmds.spawn((
				BtnState::default(),
				size,
				SpriteSheetBundle {
					transform: Transform::from_xyz(60.0, -72.0, 951.0),
					texture_atlas: btn,
					..default()
				},
				IsNoBtn,
			));

			cmds.spawn(Text2dBundle {
				text: Text::from_section(
					"Yes",
					TextStyle {
						font: font.clone(),
						font_size: 24.0,
						..default()
					},
				),
				transform: Transform::from_xyz(-60.0, -70.0, 952.0),
				..default()
			});

			cmds.spawn(Text2dBundle {
				text: Text::from_section(
					"No",
					TextStyle {
						font,
						font_size: 24.0,
						..default()
					},
				),
				transform: Transform::from_xyz(60.0, -70.0, 952.0),
				..default()
			});
		}
	}
}

pub fn handle_really_quit_btn(
	q: Query<&BtnState, (Changed<BtnState>, With<IsYesBtn>)>,
	mut exit_events: EventWriter<AppExit>,
) {
	for state in &q {
		if *state == BtnState::Pressed {
			exit_events.send(AppExit);
		}
	}
}

pub fn handle_cancel_quit_btn(
	mut cmds: Commands,
	assets: Res<AssetServer>,
	texture_atlases: ResMut<Assets<TextureAtlas>>,
	q: Query<&BtnState, (Changed<BtnState>, With<IsNoBtn>)>,
	really_quit_menu: Res<ReallyQuitMenu>,
	volume: Res<GlobalVolume>,
) {
	if let Ok(state) = q.get_single() {
		if *state == BtnState::Pressed {
			really_quit_menu.despawn(&mut cmds);
			spawn_pause_menu(cmds, assets, texture_atlases, volume);
		}
	}
}

pub fn handle_play_btn(
	q: Query<&BtnState, (Changed<BtnState>, With<IsOkBtn>)>,
	mut app_state: ResMut<NextState<AppState>>,
) {
	if let Ok(state) = q.get_single() {
		if *state == BtnState::Pressed {
			app_state.set(AppState::UnPaused);
		}
	}
}

pub fn slide_slider(
	mut q: Query<(&mut Transform, &BtnState), With<IsSliderHandle>>,
	window: Query<&Window, With<PrimaryWindow>>,
	camera: Query<(&Camera, &GlobalTransform)>,
	mut volume: ResMut<GlobalVolume>,
) {
	if let Some(pos) = window.single().cursor_position() {
		let (cam, cam_xform) = camera.single();
		let pos = cam
			.viewport_to_world(cam_xform, pos)
			.expect("mouse pos to world coords")
			.origin
			.truncate();
		for (mut xform, state) in &mut q {
			if *state == BtnState::Pressed {
				xform.translation.x = pos.x.clamp(-108.0, 108.0);
				volume.volume = VolumeLevel::new((xform.translation.x + 108.0) / 216.0);
			}
		}
	}
}

pub fn update_already_playing_volume(mut q: Query<&mut AudioSink>, volume: Res<GlobalVolume>) {
	if volume.is_changed() {
		for sink in &mut q {
			sink.set_volume(volume.volume.get());
		}
	}
}

#[derive(Component, Default, Debug, Deref, DerefMut, Copy, Clone)]
pub struct BtnSize(Vec2);
