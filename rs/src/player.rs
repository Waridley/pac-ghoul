use crate::arena::{Arena, Bean, TileResident, TILE_SIZE};
use crate::audio::Crunches;
use crate::collision::{Collider, DynamicBundle, Sensor};
use crate::ghosts::Ghost;
use crate::scoreboard::Score;
use crate::sim::{SimPos, Vel, COLLISION_EPSILON};
use bevy::audio::{PlaybackMode, Volume, VolumeLevel};

use bevy::prelude::*;

pub const SPEED: f32 = 2.0;

pub fn setup(
	mut cmds: Commands,
	assets: ResMut<AssetServer>,
	mut texture_atlases: ResMut<Assets<TextureAtlas>>,
) {
	let texture = assets.load("evil_pacman.png");
	let texture_atlas = TextureAtlas::from_grid(texture, Vec2::new(16.0, 16.0), 5, 1, None, None);
	let texture_atlas = texture_atlases.add(texture_atlas);

	cmds.insert_resource(RespawnData {
		timer: None,
		texture_atlas: texture_atlas.clone(),
	});
	cmds.spawn((
		IsPlayer,
		SpriteSheetBundle {
			sprite: TextureAtlasSprite::new(4),
			texture_atlas,
			..default()
		},
		DynamicBundle {
			collider: Collider::circle(8.0 / TILE_SIZE.x),
			position: SimPos::from_tile_index(0, 0),
			..default()
		},
	));
}

#[derive(Component)]
pub struct IsPlayer;

pub fn input(keys: Res<Input<KeyCode>>, mut player_q: Query<&mut Vel, With<IsPlayer>>) {
	let Ok(mut v) = player_q.get_single_mut() else {
		return;
	};

	let up = keys.any_pressed([KeyCode::W, KeyCode::Up]) as u8 as f32;
	let down = keys.any_pressed([KeyCode::S, KeyCode::Down]) as u8 as f32;
	let left = keys.any_pressed([KeyCode::A, KeyCode::Left]) as u8 as f32;
	let right = keys.any_pressed([KeyCode::D, KeyCode::Right]) as u8 as f32;

	v.x = (right - left) * SPEED;
	v.y = (up - down) * SPEED;
}

pub fn animate_pacman(
	mut q: Query<(&Vel, &mut TextureAtlasSprite, &mut Transform), With<IsPlayer>>,
	t: Res<Time>,
) {
	let Ok((v, mut atlas, mut xform)) = q.get_single_mut() else {
		return;
	};
	if **v == Vec2::ZERO {
		return;
	}

	atlas.index = ((t.elapsed().as_millis() / 100) % 5) as usize;
	if v.x > 0.0 {
		xform.scale.x = -1.0;
	} else if v.x < 0.0 {
		xform.scale.x = 1.0;
	}
}

#[derive(Resource, Deref, DerefMut, Default, Debug, Clone)]
pub struct RespawnData {
	#[deref]
	timer: Option<Timer>,
	texture_atlas: Handle<TextureAtlas>,
}

pub fn chomp_beans(
	mut cmds: Commands,
	beans: Query<(Entity, &SimPos, &Sensor, &Bean)>,
	player: Query<(&SimPos, &Collider), With<IsPlayer>>,
	mut arena: ResMut<Arena>,
	mut score: ResMut<Score>,
	crunches: Res<Crunches>,
) {
	let Ok(player) = player.get_single() else {
		return;
	};

	for bean in &beans {
		if player
			.1
			.distance_to_collide_with(**bean.2, *player.0, *bean.1)
			< -COLLISION_EPSILON
		{
			cmds.entity(bean.0).despawn();
			let tile = arena.closest_tile_to(*player.0);
			arena.grid[tile.0][tile.1].resident = TileResident::Player;
			**score += match bean.3 {
				Bean::Small => 10,
				Bean::Large => 250,
			};
			cmds.spawn(AudioBundle {
				source: crunches.random(),
				settings: PlaybackSettings {
					mode: PlaybackMode::Despawn,
					volume: Volume::Relative(VolumeLevel::new(match bean.3 {
						Bean::Small => 0.5,
						Bean::Large => 1.0,
					})),
					..default()
				},
			});
		}
	}
}

pub fn die(
	mut cmds: Commands,
	mut player: Query<(Entity, &SimPos, &Collider), With<IsPlayer>>,
	ghosts: Query<(&SimPos, &Sensor), With<Ghost>>,
	mut respawn_timer: ResMut<RespawnData>,
	mut score: ResMut<Score>,
) {
	let Ok((id, pos, col)) = player.get_single_mut() else {
		return;
	};
	for (ghost_pos, ghost_col) in &ghosts {
		if col.distance_to_collide_with(**ghost_col, *pos, *ghost_pos) < COLLISION_EPSILON {
			cmds.entity(id).despawn();
			respawn_timer.timer = Some(Timer::from_seconds(5.0, TimerMode::Once));
			**score = ((**score / 10) / 2) * 10;
		}
	}
}

pub fn respawn(mut cmds: Commands, mut respawn_data: ResMut<RespawnData>, t: Res<Time>) {
	let Some(timer) = respawn_data.timer.as_mut() else {
		return;
	};
	timer.tick(t.delta());
	if timer.just_finished() {
		cmds.spawn((
			IsPlayer,
			SpriteSheetBundle {
				sprite: TextureAtlasSprite::new(4),
				texture_atlas: respawn_data.texture_atlas.clone(),
				..default()
			},
			DynamicBundle {
				collider: Collider::circle(8.0 / TILE_SIZE.x),
				position: SimPos::from_tile_index(0, 0),
				..default()
			},
		));
	}
}
