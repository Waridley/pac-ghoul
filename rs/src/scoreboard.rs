use bevy::prelude::*;
use bevy::sprite::Anchor;

pub fn setup(mut cmds: Commands, assets: ResMut<AssetServer>) {
	cmds.insert_resource(Score(0));

	let font = assets.load("VT323-Regular.ttf");
	let large_bean = assets.load("score_bean.png");
	cmds.spawn((SpriteBundle {
		texture: large_bean,
		transform: Transform::from_xyz(-204.0, 132.0, 0.0),
		..default()
	},));

	cmds.spawn((
		Text2dBundle {
			text: Text::from_section(
				"0",
				TextStyle {
					font_size: 24.0,
					font: font.clone(),
					..default()
				},
			),
			text_anchor: Anchor::TopRight,
			transform: Transform::from_xyz(-216.0, 144.0, 0.0),
			..default()
		},
		IsScoreText,
	));

	cmds.spawn(Text2dBundle {
		text: Text::from_section(
			"Move:\n\
			\x20WASD or Arrow Keys\n\
			Pause:\n\
			\x20Escape or Enter",
			TextStyle {
				font_size: 16.0,
				font: font.clone(),
				..default()
			},
		),
		text_anchor: Anchor::TopLeft,
		transform: Transform::from_xyz(-316.0, 96.0, 0.0),
		..default()
	});
	cmds.spawn(Text2dBundle {
		text: Text::from_section(
			"Collect beans:\n\
			\x20\x20Small: 10 points\n\
			\x20\x20Large: 250 points\n\
			\n\
			Touch a ghost:\n\
			\x20\x20Lose half of score\n\
			\x20\x20Resapawn after 5s",
			TextStyle {
				font_size: 12.0,
				font,
				..default()
			},
		),
		text_anchor: Anchor::TopLeft,
		transform: Transform::from_xyz(-316.0, 24.0, 0.0),
		..default()
	});
}

pub fn update_score_text(mut q: Query<&mut Text, With<IsScoreText>>, score: Res<Score>) {
	if score.is_changed() {
		q.single_mut()
			.sections
			.first_mut()
			.expect("get score text section") // Score text should never be removed, game should crash if its gone
			.value = format!("{}", score.0);
	}
}

#[derive(Resource, Default, Debug, Deref, DerefMut, PartialEq, PartialOrd, Eq, Ord)]
pub struct Score(pub u32);

#[derive(Component)]
pub struct IsScoreText;
