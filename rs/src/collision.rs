use crate::sim::{SimPos, Vel};
use bevy::prelude::*;

#[derive(Bundle, Default, Debug)]
pub struct DynamicBundle {
	pub collider: Collider,
	pub position: SimPos,
	pub velocity: Vel,
	pub next_pos: NextPos,
}

#[derive(Bundle, Default, Debug)]
pub struct DynamicSensorBundle {
	pub sensor: Sensor,
	pub position: SimPos,
	pub velocity: Vel,
	pub next_pos: NextPos,
}

#[derive(Bundle, Default, Debug)]
pub struct KinematicBundle {
	pub collider: Collider,
	pub position: SimPos,
	pub velocity: Vel,
	pub next_pos: NextPos,
	pub kinematic: Kinematic,
}

#[derive(Component, Default, Debug)]
pub struct Kinematic;

#[derive(Component, Default, Debug, Copy, Clone, Deref, DerefMut)]
pub struct NextPos(pub SimPos);

#[derive(Component, Copy, Clone, Debug)]
pub enum Collider {
	Circle {
		/// Radius in tiles
		r: f32,
	},
	Rect {
		/// Width in tiles
		x: f32,
		/// Height in tiles
		y: f32,
	},
}

#[derive(Component, Default, Copy, Clone, Debug, Deref, DerefMut)]
pub struct Sensor(pub Collider);

impl Default for Collider {
	fn default() -> Self {
		Self::square(1.0)
	}
}

impl Collider {
	pub const fn circle(r: f32) -> Self {
		Self::Circle { r }
	}

	pub const fn square(size: f32) -> Self {
		Self::Rect { x: size, y: size }
	}

	pub const fn rect(x: f32, y: f32) -> Self {
		Self::Rect { x, y }
	}

	pub fn width(self) -> f32 {
		match self {
			Collider::Circle { r } => r * 2.0,
			Collider::Rect { x, .. } => x,
		}
	}

	pub fn height(self) -> f32 {
		match self {
			Collider::Circle { r } => r * 2.0,
			Collider::Rect { y, .. } => y,
		}
	}

	pub fn max_dim(self) -> f32 {
		match self {
			Collider::Circle { r } => r,
			Collider::Rect { x, y } => f32::max(x, y),
		}
	}

	pub fn min_dim(self) -> f32 {
		match self {
			Collider::Circle { r } => r,
			Collider::Rect { x, y } => f32::min(x, y),
		}
	}

	pub fn distance_to_collide_with(self, other: Collider, pos: SimPos, other_pos: SimPos) -> f32 {
		// TODO: Return a Vec2 instead
		use Collider::*;

		fn circle_to_rect(r: f32, x: f32, y: f32, cpos: SimPos, rpos: SimPos) -> f32 {
			let dist_x = rpos.x - cpos.x;
			let dist_y = rpos.y - cpos.y;
			if dist_x.abs() > x * 0.5 && dist_y.abs() > y * 0.5 {
				let corner_x = rpos.x + (-dist_x.signum() * x * 0.5);
				let corner_y = rpos.y + (-dist_y.signum() * y * 0.5);
				cpos.distance(Vec2::new(corner_x, corner_y)) - r
			} else {
				f32::max(dist_x.abs() - (x * 0.5) - r, dist_y.abs() - (y * 0.5) - r)
			}
		}

		match (self, other) {
			(Circle { r: a }, Circle { r: b }) => {
				let center_dist = pos.distance(*other_pos);
				center_dist - a - b
			}
			(Rect { x: ax, y: ay }, Rect { x: bx, y: by }) => {
				let x_dist = (pos.x - other_pos.x).abs() - (ax * 0.5) - (bx * 0.5);
				let y_dist = (pos.y - other_pos.y).abs() - (ay * 0.5) - (by * 0.5);
				f32::max(x_dist, y_dist)
			}
			(Circle { r }, Rect { x, y }) => circle_to_rect(r, x, y, pos, other_pos),
			(Rect { x, y }, Circle { r }) => circle_to_rect(r, x, y, other_pos, pos),
		}
	}
}

#[derive(Event, Clone, Debug)]
pub struct Collision {
	pub entity_1: Entity,
	pub entity_2: Entity,
}
