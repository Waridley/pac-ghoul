use crate::arena::{TileIndex, COLUMNS, ROWS, TILE_SIZE};
use crate::collision::{Collider, Collision, Kinematic, NextPos};
use bevy::prelude::*;

pub const COLLISION_EPSILON: f32 = 0.02;
pub const CMP_EPSILON: f32 = 1.0e-6;

#[derive(Component, Deref, DerefMut, Default, Copy, Clone, Debug, PartialEq)]
pub struct SimPos(pub Vec2);

impl SimPos {
	pub fn new(x: f32, y: f32) -> Self {
		Self(Vec2::new(x, y))
	}

	pub fn from_tile_index(x: usize, y: usize) -> Self {
		Self::new(
			x as f32 - ((COLUMNS - 1) as f32 / 2.0),
			y as f32 - ((ROWS - 1) as f32 / 2.0),
		)
	}
}

impl From<TileIndex> for SimPos {
	fn from(value: TileIndex) -> Self {
		Self::from_tile_index(value.0, value.1)
	}
}

#[derive(Component, Deref, DerefMut, Default, Copy, Clone, Debug)]
pub struct Vel(pub Vec2);

impl Vel {
	pub fn new(x: f32, y: f32) -> Self {
		Self(Vec2::new(x, y))
	}
}

pub fn refresh_xforms(mut q: Query<(&SimPos, &mut Transform), Changed<SimPos>>) {
	for (sim_xform, mut xform) in &mut q {
		xform.translation.x = sim_xform.x * TILE_SIZE.x;
		xform.translation.y = sim_xform.y * TILE_SIZE.y;
	}
}

pub fn apply_vel(
	t: Res<Time>,
	mut no_colliders: Query<(Entity, &Vel, &mut SimPos), Without<Collider>>,
	mut dynamic_colliders: Query<(
		Entity,
		&Vel,
		&mut SimPos,
		&Collider,
		&mut NextPos,
		Option<&Kinematic>,
	)>,
	static_colliders: Query<(Entity, &SimPos, &Collider), Without<Vel>>,
	mut collision_events: EventWriter<Collision>,
) {
	let t = t.delta_seconds();

	for (_, v, mut pos) in &mut no_colliders {
		**pos += **v * t;
	}

	// Dynamic <-> Static collisions
	dynamic_colliders.for_each_mut(|(entity_1, v, pos, col, mut next_pos, _)| {
		if v.length_squared() < CMP_EPSILON {
			**next_pos = *pos;
			return;
		}

		let test_pos = SimPos(**pos + (**v * t));
		for (entity_2, other_pos, other) in &static_colliders {
			let dist = col.distance_to_collide_with(*other, test_pos, *other_pos);
			if dist < 0.0 {
				collision_events.send(Collision { entity_1, entity_2 });
				let attempted_delta = *test_pos - **pos;
				let available_dist = attempted_delta.length() + dist - COLLISION_EPSILON;
				***next_pos = **pos + (attempted_delta * available_dist);
				return;
			}
		}
		***next_pos = **pos + (**v * t);
	});

	// Dynamic <-> Dynamic collisions
	let mut combinations = dynamic_colliders.iter_combinations_mut();
	while let Some(
		[(entity_1, _, pos1, col1, mut next_pos1, kin1), (entity_2, _, pos2, col2, mut next_pos2, kin2)],
	) = combinations.fetch_next()
	{
		if ***next_pos1 == **pos1 && ***next_pos2 == **pos2 {
			return;
		}
		let dist = col1.distance_to_collide_with(*col2, **next_pos1, **next_pos2);
		if dist < 0.0 {
			collision_events.send(Collision { entity_1, entity_2 });
			match (kin1, kin2) {
				(Some(_), None) => ***next_pos2 = **pos2 + (***next_pos1 - **pos1),
				(None, Some(_)) => ***next_pos1 = **pos1 + (***next_pos2 - **pos2),
				_ => {
					**next_pos1 = *pos1;
					**next_pos2 = *pos2;
				}
			}
		}
	}

	for (_, _, mut pos, _, next_pos, _) in &mut dynamic_colliders {
		if ***next_pos != **pos {
			*pos = **next_pos
		}
	}
}

pub fn fix_oob(mut q: Query<(&mut SimPos, &mut Vel)>) {
	const HALF_COLS: f32 = COLUMNS as f32 / 2.0;
	const HALF_ROWS: f32 = ROWS as f32 / 2.0;

	for (mut pos, mut vel) in &mut q {
		if pos.x > HALF_COLS {
			pos.x = HALF_COLS - 0.5;
			vel.x = 0.0;
		}
		if pos.x < -HALF_COLS {
			pos.x = -HALF_COLS + 0.5;
			vel.x = 0.0;
		}
		if pos.y > HALF_ROWS {
			pos.y = HALF_ROWS - 0.5;
			vel.y = 0.0;
		}
		if pos.y < -HALF_ROWS {
			pos.y = -HALF_ROWS + 0.5;
			vel.y = 0.0;
		}
	}
}
