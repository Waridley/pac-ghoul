use crate::collision::Collision;

use crate::menu::{PauseMenu, ReallyQuitMenu};
use bevy::asset::ChangeWatcher;
use bevy::core_pipeline::clear_color::ClearColorConfig;
use bevy::prelude::*;
use bevy::render::camera::ScalingMode;
use bevy::window::{PrimaryWindow, WindowResolution};
use std::path::PathBuf;
use std::time::Duration;

pub mod arena;
pub mod audio;
pub mod collision;
pub mod ghosts;
pub mod menu;
pub mod player;
pub mod scoreboard;
pub mod sim;

const SCREEN_WIDTH: f32 = 512.0;
const SCREEN_HEIGHT: f32 = 288.0;

pub fn run() {
	let mut asset_folder = std::env::var("CARGO_MANIFEST_DIR")
		.ok()
		.map(|dir| {
			let mut dir = PathBuf::from(&dir);
			dir.pop(); // assets are beside `rs` folder during dev
			dir
		})
		.unwrap_or_else(PathBuf::new);
	asset_folder.push("assets");
	let asset_folder = asset_folder.to_str().map(str::to_owned).unwrap();

	App::new()
		.add_plugins(
			DefaultPlugins
				.set(WindowPlugin {
					primary_window: Some(Window {
						title: "Pac Ghoul".to_string(),
						resolution: WindowResolution::new(SCREEN_WIDTH * 2.0, SCREEN_HEIGHT * 2.0)
							.with_scale_factor_override(2.0),
						resizable: true,
						fit_canvas_to_parent: true,
						canvas: Some("#game_canvas".into()),
						..default()
					}),
					..default()
				})
				.set(AssetPlugin {
					asset_folder,
					#[cfg(debug_assertions)]
					watch_for_changes: ChangeWatcher::with_delay(Duration::from_secs_f32(0.5)),
					#[cfg(not(debug_assertions))]
					watch_for_changes: None,
				})
				.set(ImagePlugin::default_nearest()),
		)
		.add_state::<AppState>()
		.add_systems(
			Startup,
			(
				setup,
				audio::setup,
				player::setup,
				arena::setup,
				tmp_setup,
				scoreboard::setup,
				ghosts::setup,
			),
		)
		.add_systems(
			PreUpdate,
			(player::input, arena::spawn_plants, ghosts::spawn_ghosts)
				.run_if(in_state(AppState::UnPaused)),
		)
		.add_systems(
			Update,
			(
				(
					sim::apply_vel,
					player::animate_pacman,
					arena::grow_plants,
					ghosts::face_ghosts.after(sim::apply_vel),
					ghosts::white_ghost_ai,
					ghosts::orange_ghost_ai,
					ghosts::purple_ghost_ai,
					ghosts::pink_ghost_ai,
					ghosts::blue_ghost_ai,
				)
					.run_if(in_state(AppState::UnPaused)),
				(
					menu::update_btn_state,
					menu::slide_slider.after(menu::update_btn_state),
					menu::update_already_playing_volume.after(menu::slide_slider),
					menu::update_btn_sprite.after(menu::update_btn_state),
					menu::handle_quit_btn,
					menu::handle_play_btn,
					(menu::handle_really_quit_btn, menu::handle_cancel_quit_btn)
						.run_if(resource_exists::<ReallyQuitMenu>()),
				)
					.run_if(in_state(AppState::Paused)),
			),
		)
		.add_systems(
			PostUpdate,
			(
				(
					sim::refresh_xforms,
					arena::update_residents,
					player::chomp_beans,
					scoreboard::update_score_text.after(player::chomp_beans),
					ghosts::regenerate_paths,
					player::die,
					player::respawn,
				)
					.run_if(in_state(AppState::UnPaused)),
				// (
				// ).run_if(in_state(AppState::Paused)),
			),
		)
		.add_systems(
			Last,
			(
				fullscreen,
				menu::handle_pause_buttons,
				sim::fix_oob.run_if(in_state(AppState::UnPaused)),
			),
		)
		.add_systems(OnEnter(AppState::Paused), menu::show_pause_menu)
		.add_systems(
			OnExit(AppState::Paused),
			menu::despawn_menus.run_if(resource_exists::<PauseMenu>()),
		)
		.run()
}

#[derive(States, Default, Hash, Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum AppState {
	#[default]
	Paused,
	UnPaused,
}

fn setup(mut cmds: Commands) {
	cmds.insert_resource(Msaa::Off);
	cmds.spawn(Camera2dBundle {
		projection: OrthographicProjection {
			scaling_mode: ScalingMode::AutoMin {
				min_width: SCREEN_WIDTH,
				min_height: SCREEN_HEIGHT,
			},
			..Camera2dBundle::default().projection
		},
		camera_2d: Camera2d {
			clear_color: ClearColorConfig::Custom(Color::BLACK),
		},
		transform: Transform::from_xyz(-64.0, 0.0, 0.0),
		..default()
	});

	cmds.insert_resource(Events::<Collision>::default());
}

fn fullscreen(kb: Res<Input<KeyCode>>, mut windows: Query<&mut Window, With<PrimaryWindow>>) {
	use bevy::window::WindowMode::*;

	if kb.just_pressed(KeyCode::F11) {
		let mut window = windows.single_mut();
		window.mode = match window.mode {
			Windowed => BorderlessFullscreen,
			_ => Windowed,
		}
	}
}

#[allow(unused_mut, unused)]
fn tmp_setup(mut cmds: Commands, assets: Res<AssetServer>) {}
