use crate::arena::{
	Arena, PathData, TileIndex, COLUMNS, NUM_PERIMETER_TILES, PERIMETER_INDICES, ROWS, TILE_SIZE,
};
use crate::collision::{Collider, DynamicSensorBundle, Sensor};
use crate::player::IsPlayer;
use crate::sim::{SimPos, Vel, CMP_EPSILON, COLLISION_EPSILON};

use bevy::prelude::*;
use petgraph::dot::{Config, Dot};

use rand::prelude::IteratorRandom;
use rand::{thread_rng, Rng};
use std::marker::PhantomData;
use std::time::Duration;

pub fn setup(mut cmds: Commands, assets: ResMut<AssetServer>) {
	let white_ghost_sprite = assets.load("white_ghost.png");
	cmds.insert_resource(GhostSpawnData::<WhiteGhost>::new(15.0, white_ghost_sprite));

	let orange_ghost_sprite = assets.load("orange_ghost.png");
	cmds.insert_resource(GhostSpawnData::<OrangeGhost>::new(
		45.0,
		orange_ghost_sprite,
	));

	let purple_ghost_sprite = assets.load("purple_ghost.png");
	cmds.insert_resource(GhostSpawnData::<PurpleGhost>::new(
		95.0,
		purple_ghost_sprite,
	));

	let pink_ghost_sprite = assets.load("pink_ghost.png");
	cmds.insert_resource(GhostSpawnData::<PinkGhost>::new(155.0, pink_ghost_sprite));

	let blue_ghost_sprite = assets.load("blue_ghost.png");
	cmds.insert_resource(GhostSpawnData::<BlueGhost>::new(245.0, blue_ghost_sprite));
}

pub fn spawn_ghosts(
	mut cmds: Commands,
	mut white_data: ResMut<GhostSpawnData<WhiteGhost>>,
	mut orange_data: ResMut<GhostSpawnData<OrangeGhost>>,
	mut purple_data: ResMut<GhostSpawnData<PurpleGhost>>,
	mut pink_data: ResMut<GhostSpawnData<PinkGhost>>,
	mut blue_data: ResMut<GhostSpawnData<BlueGhost>>,
	player: Query<&SimPos, With<IsPlayer>>,
	path_data: Res<PathData>,
	t: Res<Time>,
) {
	let t = t.delta();
	let mut rng = thread_rng();

	white_data.tick(t);
	let Ok(player) = player.get_single() else {
		return;
	};
	if white_data.just_finished() {
		let Some(TileIndex(x, y)) = rand_tile_min_dist_from_pos(&path_data, 4.0, *player) else {
			eprintln!("failed to get tile at least 4.0 from {player:?}");
			return;
		};
		println!("Spawning ghost at {{ {x}, {y} }}");
		cmds.spawn((
			SpriteBundle {
				texture: white_data.1.clone(),
				transform: Transform::from_xyz(0.0, 0.0, 1.0),
				..default()
			},
			DynamicSensorBundle {
				sensor: Sensor(Collider::Circle {
					r: (6.0 / TILE_SIZE.x),
				}),
				position: SimPos::from_tile_index(x, y),
				..default()
			},
			GoalPos::default(),
			CurrPath::default(),
			Ghost,
			WhiteGhost,
		));
	}

	orange_data.tick(t);
	if orange_data.just_finished() {
		let Some(TileIndex(x, y)) = rand_tile_min_dist_from_pos(&path_data, 5.0, *player) else {
			eprintln!("failed to get tile at least 5.0 from {player:?}");
			return;
		};
		println!("Spawning ghost at {{ {x}, {y} }}");
		cmds.spawn((
			SpriteBundle {
				texture: orange_data.1.clone(),
				transform: Transform::from_xyz(0.0, 0.0, 1.0),
				..default()
			},
			DynamicSensorBundle {
				sensor: Sensor(Collider::Circle {
					r: (6.0 / TILE_SIZE.x),
				}),
				position: SimPos::from_tile_index(x, y),
				..default()
			},
			GoalPos::default(),
			CurrPath::default(),
			RecomputePathTimer(Timer::from_seconds(2.0, TimerMode::Once)),
			Ghost,
			OrangeGhost,
		));
	}

	purple_data.tick(t);
	if purple_data.just_finished() {
		let Some((x, y)) = &path_data.nodes().choose(&mut rng) else {
			eprintln!("failed to get random tile for purple ghost to spawn at");
			return;
		};
		println!("Spawning ghost at {{ {x}, {y} }}");
		cmds.spawn((
			SpriteBundle {
				texture: purple_data.1.clone(),
				transform: Transform::from_xyz(0.0, 0.0, 1.0),
				..default()
			},
			SimPos::from_tile_index(*x, *y),
			PurpleFadeTimer::In(Timer::from_seconds(2.0, TimerMode::Once)),
			Ghost,
			PurpleGhost,
		));
	}

	pink_data.tick(t);
	if pink_data.just_finished() {
		let Some((x, y)) = PERIMETER_INDICES
			.iter()
			.copied()
			.filter(|(x, y)| Vec2::new(*x as f32, *y as f32).distance(**player) > 5.0)
			.choose(&mut rng)
		else {
			eprintln!("failed to get tile at least 5.0 from {player:?}");
			return;
		};
		println!("Spawning ghost at {{ {x}, {y} }}");
		cmds.spawn((
			SpriteBundle {
				texture: pink_data.1.clone(),
				transform: Transform::from_xyz(0.0, 0.0, 1.0),
				..default()
			},
			DynamicSensorBundle {
				sensor: Sensor(Collider::Circle {
					r: (6.0 / TILE_SIZE.x),
				}),
				position: SimPos::from_tile_index(x, y),
				..default()
			},
			GoalPos(SimPos::from_tile_index(x, y)),
			RecomputePathTimer(Timer::from_seconds(1.0, TimerMode::Once)),
			Ghost,
			PinkGhost,
		));
	}

	blue_data.tick(t);
	if blue_data.just_finished() {
		let Some(TileIndex(x, y)) =
			rand_tile_min_dist_from_pos(&path_data, COLUMNS as f32 / 2.0, *player)
		else {
			eprintln!(
				"failed to get tile at least {} from {player:?}",
				COLUMNS as f32 / 2.0
			);
			return;
		};
		println!("Spawning ghost at {{ {x}, {y} }}");
		cmds.spawn((
			SpriteBundle {
				texture: blue_data.1.clone(),
				transform: Transform::from_xyz(0.0, 0.0, 1.0),
				..default()
			},
			DynamicSensorBundle {
				sensor: Sensor(Collider::Circle {
					r: (6.0 / TILE_SIZE.x),
				}),
				position: SimPos::from_tile_index(x, y),
				..default()
			},
			GoalPos::default(),
			CurrPath::default(),
			RecomputePathTimer(Timer::from_seconds(3.0, TimerMode::Once)),
			Ghost,
			BlueGhost,
		));
	}
}

#[derive(Resource, Deref, DerefMut)]
pub struct GhostSpawnData<GhostColor>(#[deref] Timer, Handle<Image>, PhantomData<GhostColor>);

impl<GhostColor> GhostSpawnData<GhostColor> {
	pub fn new(seconds: f32, sprite: Handle<Image>) -> Self {
		Self(
			Timer::from_seconds(seconds, TimerMode::Once),
			sprite,
			PhantomData::default(),
		)
	}
}

#[derive(Component)]
pub struct Ghost;
#[derive(Component)]
pub struct WhiteGhost;
#[derive(Component)]
pub struct OrangeGhost;
#[derive(Component)]
pub struct PinkGhost;
#[derive(Component)]
pub struct BlueGhost;
#[derive(Component)]
pub struct RedGhost;
#[derive(Component)]
pub struct PurpleGhost;

pub fn white_ghost_ai(
	mut q: Query<(&mut SimPos, &mut Vel, &mut GoalPos, &mut CurrPath), With<WhiteGhost>>,
	path_data: Res<PathData>,
) {
	for (pos, mut vel, mut goal_pos, mut curr_path) in &mut q {
		if *pos == **goal_pos || curr_path.is_empty() {
			let Some(goal) = rand_tile_min_dist_from_pos(&path_data, 1.0, *pos) else {
				eprintln!("failed to get tile at least 1.0 from {pos:?}");
				return;
			};
			**goal_pos = SimPos::from(goal);
			continue;
		}
		let next_target = curr_path.first().expect("get first node of path"); // already checked that it's not empty
		let next_target = SimPos::from_tile_index(next_target.0, next_target.1);
		if (*next_target - **pos).length() < COLLISION_EPSILON {
			curr_path.remove(0);
		} else {
			let x_dir = next_target.x - pos.x;
			vel.x = 0.5
				* if x_dir.abs() < 0.01 {
					0.0
				} else {
					x_dir.signum()
				};
			let y_dir = next_target.y - pos.y;
			vel.y = 0.5
				* if y_dir.abs() < 0.01 {
					0.0
				} else {
					y_dir.signum()
				};
		}
	}
}

pub fn orange_ghost_ai(
	mut q: Query<
		(
			&mut SimPos,
			&mut Vel,
			&mut GoalPos,
			&mut CurrPath,
			&mut RecomputePathTimer,
		),
		With<OrangeGhost>,
	>,
	player: Query<&SimPos, (With<IsPlayer>, Without<OrangeGhost>)>,
	arena: Res<Arena>,
	t: Res<Time>,
) {
	for (pos, mut vel, mut goal_pos, mut curr_path, mut timer) in &mut q {
		timer.tick(t.delta());
		if timer.just_finished() || *pos == **goal_pos || curr_path.is_empty() {
			timer.reset();
			let Ok(player) = player.get_single() else {
				**goal_pos = TileIndex(COLUMNS - 1, ROWS - 1).into();
				continue;
			};
			let end = arena.closest_tile_to(*player);
			**goal_pos = end.into();
			continue;
		}
		let next_target = curr_path.first().expect("get first node of path"); // already checked that it's not empty
		let next_target = SimPos::from_tile_index(next_target.0, next_target.1);
		if (*next_target - **pos).length() < COLLISION_EPSILON {
			curr_path.remove(0);
		} else {
			let x_dir = next_target.x - pos.x;
			vel.x = 0.5
				* if x_dir.abs() < 0.01 {
					0.0
				} else {
					x_dir.signum()
				};
			let y_dir = next_target.y - pos.y;
			vel.y = 0.5
				* if y_dir.abs() < 0.01 {
					0.0
				} else {
					y_dir.signum()
				};
		}
	}
}

pub fn purple_ghost_ai(
	mut cmds: Commands,
	mut q: Query<(Entity, &mut Sprite, &mut PurpleFadeTimer), With<PurpleGhost>>,
	mut respawn_timer: ResMut<GhostSpawnData<PurpleGhost>>,
	t: Res<Time>,
) {
	let t = t.delta();
	for (id, mut sprite, mut timer) in &mut q {
		let new_timer = match std::mem::replace(&mut *timer, PurpleFadeTimer::None) {
			PurpleFadeTimer::In(mut timer) => {
				timer.tick(t);
				let timer_elapsed_percent = timer.elapsed_secs() / 2.0;
				sprite
					.color
					.set_a(timer_elapsed_percent * timer_elapsed_percent);
				if timer.finished() {
					cmds.entity(id).insert(Sensor(Collider::Circle {
						r: (6.0 / TILE_SIZE.x),
					}));
					PurpleFadeTimer::Wait(Timer::from_seconds(
						thread_rng().gen_range(1.0..5.0),
						TimerMode::Once,
					))
				} else {
					PurpleFadeTimer::In(timer)
				}
			}
			PurpleFadeTimer::Wait(mut timer) => {
				timer.tick(t);
				if timer.finished() {
					PurpleFadeTimer::Out(Timer::from_seconds(2.0, TimerMode::Once))
				} else {
					PurpleFadeTimer::Wait(timer)
				}
			}
			PurpleFadeTimer::Out(mut timer) => {
				timer.tick(t);
				sprite.color.set_a(1.0 - (timer.elapsed_secs() / 2.0));
				if timer.finished() {
					cmds.entity(id).despawn();
					respawn_timer
						.set_duration(Duration::from_secs_f32(thread_rng().gen_range(3.0..8.0)));
					respawn_timer.reset();
					PurpleFadeTimer::None
				} else {
					PurpleFadeTimer::Out(timer)
				}
			}
			none => none,
		};
		*timer = new_timer;
	}
}

pub fn pink_ghost_ai(
	mut q: Query<(&mut SimPos, &mut Vel, &mut GoalPos, &mut RecomputePathTimer), With<PinkGhost>>,
	arena: Res<Arena>,
	t: Res<Time>,
) {
	for (pos, mut vel, mut goal_pos, mut timer) in &mut q {
		let tile = arena.closest_tile_to(*pos);
		let curr_index: usize = PERIMETER_INDICES
			.iter()
			.enumerate()
			.find(|(_, val)| **val == (tile.0, tile.1))
			.unwrap_or((0, &(0, 0)))
			.0;
		let goal_tile = arena.closest_tile_to(**goal_pos);
		let goal_index: usize = PERIMETER_INDICES
			.iter()
			.enumerate()
			.find(|(_, val)| **val == (goal_tile.0, goal_tile.1))
			.unwrap_or((0, &(0, 0)))
			.0;
		timer.tick(t.delta());
		if timer.just_finished() {
			let mut rng = thread_rng();
			timer.set_duration(Duration::from_secs_f32(rng.gen_range(3.0..8.0)));
			timer.reset();
			let goal_dist = rng.gen_range(-8..8);
			let goal_index = (NUM_PERIMETER_TILES + curr_index)
				.checked_add_signed(goal_dist)
				.expect("goal_index offset") // if it overflows I did something wrong
				% NUM_PERIMETER_TILES;
			let goal_tile = PERIMETER_INDICES[goal_index];
			dbg!(goal_tile);
			**goal_pos = SimPos::from_tile_index(goal_tile.0, goal_tile.1);
		}
		const HALF_PERIM: isize = ((COLUMNS - 1) + (ROWS - 1)) as isize;
		let diff = (goal_index as isize) - (curr_index as isize);
		let diff = if diff >= HALF_PERIM {
			-(NUM_PERIMETER_TILES as isize) - diff
		} else if diff <= -HALF_PERIM {
			NUM_PERIMETER_TILES as isize - diff
		} else {
			diff
		};
		let dir = diff.signum();
		let next_tile = (curr_index + NUM_PERIMETER_TILES)
			.checked_add_signed(dir)
			.expect("next_tile calculation") // if it overflows I did something wrong
			% NUM_PERIMETER_TILES;
		let next_tile = PERIMETER_INDICES[next_tile];
		let next_pos = SimPos::from_tile_index(next_tile.0, next_tile.1);
		let dir = (*next_pos - **pos).normalize_or_zero();
		let dist = ***goal_pos - **pos;
		let manhattan_dist = dist.x.abs() + dist.y.abs();
		let speed = if manhattan_dist > COLLISION_EPSILON {
			(manhattan_dist * 0.3) + 1.0
		} else {
			0.0
		};
		**vel = dir * speed;
	}
}

pub fn blue_ghost_ai(
	mut q: Query<
		(
			&mut SimPos,
			&mut Vel,
			&mut GoalPos,
			&mut CurrPath,
			&mut RecomputePathTimer,
		),
		With<BlueGhost>,
	>,
	player: Query<&SimPos, (With<IsPlayer>, Without<BlueGhost>)>,
	path_data: Res<PathData>,
	t: Res<Time>,
) {
	let Ok(player) = player.get_single() else {
		return;
	};
	for (pos, mut vel, mut goal_pos, mut curr_path, mut timer) in &mut q {
		timer.tick(t.delta());
		let recalc = timer.just_finished() && player.distance(***goal_pos) < COLUMNS as f32 / 2.0;
		if recalc || *pos == **goal_pos || curr_path.is_empty() {
			timer.reset();
			let Some(goal) = rand_tile_min_dist_from_pos(&path_data, COLUMNS as f32 / 2.0, *player)
			else {
				eprintln!(
					"failed to get tile at least {} from {player:?}",
					COLUMNS as f32 / 2.0
				);
				return;
			};
			**goal_pos = SimPos::from(goal);
			continue;
		}
		let next_target = curr_path.first().expect("get first node of path"); // already checked that it's not empty
		let next_target = SimPos::from_tile_index(next_target.0, next_target.1);
		if (*next_target - **pos).length() < COLLISION_EPSILON {
			curr_path.remove(0);
		} else {
			let x_dir = next_target.x - pos.x;
			vel.x = 0.8
				* if x_dir.abs() < 0.01 {
					0.0
				} else {
					x_dir.signum()
				};
			let y_dir = next_target.y - pos.y;
			vel.y = 0.8
				* if y_dir.abs() < 0.01 {
					0.0
				} else {
					y_dir.signum()
				};
		}
	}
}

#[derive(Component, Deref, DerefMut, Debug)]
pub struct RecomputePathTimer(Timer);

#[derive(Component, Debug)]
pub enum PurpleFadeTimer {
	None,
	In(Timer),
	Wait(Timer),
	Out(Timer),
}

#[derive(Component, Deref, DerefMut, Default, Debug, Copy, Clone)]
pub struct GoalPos(SimPos);

#[derive(Component, Default, Debug, Deref, DerefMut)]
pub struct CurrPath(Vec<(usize, usize)>);

pub fn rand_tile_min_dist_from_pos(
	path_data: &PathData,
	dist: f32,
	pos: SimPos,
) -> Option<TileIndex> {
	let mut tmp = path_data.clone();
	for (x, y) in path_data.nodes() {
		let d = pos.distance(*SimPos::from_tile_index(x, y));
		if d < dist {
			tmp.remove_node((x, y));
		}
	}
	let (x, y) = tmp.nodes().choose(&mut thread_rng())?;
	Some(TileIndex(x, y))
}

pub fn regenerate_paths(
	mut q: Query<(&SimPos, &mut CurrPath, &mut GoalPos)>,
	arena: Res<Arena>,
	path_data: Res<PathData>,
) {
	let data_changed = path_data.is_changed();

	for (pos, mut curr_path, mut goal_pos) in &mut q {
		if !data_changed && !goal_pos.is_changed() {
			continue;
		}

		let start = arena.closest_tile_to(*pos);
		let end = arena.closest_tile_to(**goal_pos);
		if !path_data.contains_node((end.0, end.1)) {
			**goal_pos = *pos;
			curr_path.clear();
			continue;
		}
		if start == end {
			continue;
		}
		let path = petgraph::algo::astar(
			&**path_data,
			(start.0, start.1),
			|id| id == (end.0, end.1),
			|_| 1,
			|id| id.0.abs_diff(end.0) + id.1.abs_diff(end.1),
		);
		if let Some((cost, mut path)) = path {
			path.remove(0);
			if path != **curr_path {
				println!("New path: (cost: {cost}) {path:?}");
				**curr_path = path;
			}
		} else {
			eprintln!("Failed to find path from {start:?} to {end:?}");
			#[cfg(not(target_arch = "wasm32"))]
			std::fs::write(
				"failed_astar.txt",
				format!(
					"Failed to find path from {start:?} to {end:?}\n{:?}",
					Dot::with_config(&**path_data, &[Config::EdgeNoLabel])
				),
			)
			.map_err(|e| eprintln!("{e}"))
			.ok();
		}
	}
}

pub fn face_ghosts(mut q: Query<(&mut Transform, &Vel), With<Ghost>>) {
	for (mut xform, v) in &mut q {
		if v.x > CMP_EPSILON {
			xform.scale.x = 1.0;
		} else if v.x < -CMP_EPSILON {
			xform.scale.x = -1.0;
		}
	}
}
